#include <iostream>

using namespace std;

/**
 *  Project Euler Problem 1:
 *  Obtain the sum of all multiples of 3 and 5 below 1000 (as in up to 999).
 *  Sean Castillo
 */

const unsigned int N = 999;

int main()
{
    // Triangle number for 3s
    unsigned int S3  = 3*(((N/3)   * (N/3 + 1)) / 2 );

    // Triangle number for 5s
    unsigned int S5  = 5*(((N/5)   * (N/5 + 1)) / 2 );

    // Triangle number for 15s (common multiples)
    unsigned int S15 = 15*(((N/15) * (N/15 + 1)) / 2);

    // Total is S3 + S5 with double-counting corrected.
    cout << "The sum of all multiples of 3 and 5 below 1000 is: " << S3 + S5 - S15 << endl;

    return 0;
}
